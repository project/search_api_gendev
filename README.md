CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Search API Generic Devel module provides a new tab in the entity devel tab
group.

It has the following features:

 * The indexed data.
   * Fields indexed.
   * Extra data (server dependant).
   * Query time.
 * Locally generated data (data to be indexed on next reindex).
 * Reindex (or index) an entity manually.
 * Delete an entity index item.


REQUIREMENTS
------------

This module requires the following modules:

 * [Devel](https://www.drupal.org/project/devel)
 * [Search API](https://www.drupal.org/project/search_api)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.


MAINTAINERS
-----------

 * Philip Birk-Jensen (Birk) - https://www.drupal.org/u/birk

Supporting organization:

 * B14 - https://b14.dk
