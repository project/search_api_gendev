<?php

namespace Drupal\search_api_gendev;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\search_api\Event\IndexingItemsEvent;
use Drupal\search_api\Event\SearchApiEvents;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Utility\Utility;

/**
 * Search API Generic devel service class.
 */
class SearchApiGendev {
  /**
   * Entity Type Manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Language manager instance.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Construct.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * Create items with values to debug.
   *
   * This code is stolen from the Index::indexSpecificItems() function, so it
   * mimics the indexed data as much as possible.
   */
  public function prepareItems(IndexInterface $index, array $item_ids) {
    // @codingStandardsIgnoreStart
    /** @var \Drupal\search_api\Item\ItemInterface[] $items */
    $items = [];
    foreach ($index->loadItemsMultiple($item_ids) as $item_id => $object) {
      $items[$item_id] = \Drupal::getContainer()
        ->get('search_api.fields_helper')
        ->createItemFromObject($index, $object, $item_id);
    }

    // Preprocess the indexed items.
    $index->alterIndexedItems($items);
    $description = 'This hook is deprecated in search_api:8.x-1.14 and is removed from search_api:2.0.0. Please use the "search_api.indexing_items" event instead. See https://www.drupal.org/node/3059866';
    \Drupal::moduleHandler()->alterDeprecated($description, 'search_api_index_items', $index, $items);
    $event = new IndexingItemsEvent($index, $items);
    \Drupal::getContainer()->get('event_dispatcher')
      ->dispatch($event, SearchApiEvents::INDEXING_ITEMS);
    $items = $event->getItems();
    foreach ($items as $item) {
      // This will cache the extracted fields so processors, etc., can retrieve
      // them directly.
      $item->getFields();
    }
    $index->preprocessIndexItems($items);
    // @codingStandardsIgnoreEnd

    return $items;
  }

  /**
   * Get list of item ids.
   *
   * Returns the item ids matching the entity and settings, grouped by the
   * datasource.
   */
  protected function parseEntitySettings(EntityInterface $entity, array $settings = []) {
    $settings += [
      'indices' => [],
      'langcodes' => [],
    ];

    // Default indices to all.
    if (empty($settings['indices'])) {
      $settings['indices'] = $this->entityTypeManager->getStorage('search_api_index')->getQuery()
        ->condition('status', TRUE)
        ->execute();
    }

    // Default languages to all.
    if (empty($settings['langcodes'])) {
      $settings['langcodes'] = array_keys($this->languageManager->getLanguages());
    }

    $datasources = [];

    /** @var \Drupal\search_api\IndexInterface[] */
    $indices = $this->entityTypeManager->getStorage('search_api_index')->loadMultiple($settings['indices']);
    foreach ($indices as $index) {
      $item_ids = [];
      foreach ($index->getDatasources() as $datasource_id => $datasource) {
        if ($datasource->getEntityTypeId() !== $entity->getEntityTypeId()) {
          continue;
        }

        if ($entity instanceof TranslatableInterface) {
          foreach ($settings['langcodes'] as $langcode) {
            if ($entity->hasTranslation($langcode)) {
              $item_ids[] = $datasource->getItemId($entity->getTranslation($langcode)->getTypedData());
            }
          }
        }
        else {
          $item_ids[] = $datasource->getItemId($entity->getTypedData());
        }

        $datasources[$datasource_id] = [
          'datasource' => $datasource,
          'item_ids' => $item_ids ,
        ];
      }
    }

    return $datasources;
  }

  /**
   * Delete an entity index.
   */
  public function deleteEntity(EntityInterface $entity, array $settings = []) {
    $result = [];

    foreach ($this->parseEntitySettings($entity, $settings) as $datasource_id => $data) {
      $data['datasource']->getIndex()->trackItemsDeleted($datasource_id, $data['item_ids']);

      $item_ids = [];
      foreach ($data['item_ids'] as $item_id) {
        $item_ids[] = Utility::createCombinedId($datasource_id, $item_id);
      }
      $result = array_merge($result, $item_ids);
    }

    return $result;
  }

  /**
   * Index a single entity.
   */
  public function indexEntity(EntityInterface $entity, array $settings = []) {
    $result = [];

    foreach ($this->parseEntitySettings($entity, $settings) as $datasource_id => $data) {
      /** @var \Drupal\search_api\IndexInterface */
      $index = $data['datasource']->getIndex();

      $item_ids = [];
      foreach ($data['item_ids'] as $item_id) {
        $item_ids[] = Utility::createCombinedId($datasource_id, $item_id);
      }

      $search_objects = $index->loadItemsMultiple($item_ids);
      $result = array_merge($result, $index->indexSpecificItems($search_objects));
    }

    return $result;
  }

}
