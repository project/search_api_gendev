<?php

namespace Drupal\search_api_gendev\Controller;

use Drupal\Component\Utility\Timer;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\devel\Controller\EntityDebugController;
use Drupal\devel\DevelDumperManagerInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Utility\Utility;
use Drupal\search_api_gendev\Form\ItemForm;
use Drupal\search_api_gendev\SearchApiGendev;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for devel search api.
 */
class ItemController extends EntityDebugController {

  /**
   * Search API Gendev service instance.
   *
   * @var \Drupal\search_api_gendev\SearchApiGendev
   */
  protected $searchApiGendev;

  /**
   * ItemController constructor.
   *
   * @param \Drupal\devel\DevelDumperManagerInterface $dumper
   *   The dumper service.
   * @param \Drupal\search_api_gendev\SearchApiGendev $search_api_gendev
   *   The Search API Gendev service.
   */
  public function __construct(DevelDumperManagerInterface $dumper, SearchApiGendev $search_api_gendev) {
    $this->dumper = $dumper;
    $this->searchApiGendev = $search_api_gendev;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('devel.dumper'),
      $container->get('search_api_gendev')
    );
  }

  /**
   * View search_api entity data.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   A RouteMatch object.
   *
   * @return array
   *   Array of page elements to render.
   */
  public function entitySearchApi(RouteMatchInterface $route_match) {
    $output = [];

    $entity = $this->getEntityFromRouteMatch($route_match);
    $entity_type_id = $entity->getEntityTypeId();

    if ($entity instanceof EntityInterface) {
      /** @var \Drupal\search_api\IndexInterface[] */
      $indices = $this->entityTypeManager()->getStorage('search_api_index')->loadByProperties(['status' => TRUE]);

      foreach ($indices as $index) {
        foreach ($index->getDatasources() as $datasource) {
          if ($datasource->getEntityTypeId() !== $entity_type_id) {
            continue;
          }

          $item_id = Utility::createCombinedId($datasource->getPluginId(), $datasource->getItemId($entity->getTypedData()));

          $details = [
            '#type' => 'fieldset',
            '#title' => $item_id,
            '#open' => TRUE,
            'index' => [
              '#type' => 'item',
              '#title' => $this->t('Index'),
              '#markup' => $index->toLink()->toString(),
              '#wrapper_attributes' => ['class' => ['container-inline']],
            ]
          ];

          // Indexed data.
          Timer::start('search_api_gendev__query_index');
          $result_set = $index->query()
            ->addCondition('search_api_id', $item_id)
            ->execute();
          $time = Timer::stop('search_api_gendev__query_index');

          $details['indexed_data'] = [
            '#type' => 'details',
            '#title' => $this->t('Indexed data'),
            '#open' => TRUE,
            '#description' => $this->t('Query time: %time', ['%time' => $time['time'] . ' ms']),
          ];

          if ($result_set->getResultCount() > 0) {
            foreach ($result_set->getResultItems() as $item) {
              if ($item->getId() !== $item_id) {
                continue;
              }

              $details['indexed_data'][] = $this->getItemOutput($item);
            }
          }
          else {
            $details['indexed_data']['#title'] .= ' (' . $this->t('Not indexed') . ')';
          }

          // Local data.
          Timer::start('search_api_gendev__prepare_items');
          $items = $this->searchApiGendev->prepareItems($index, [$item_id]);
          $time = Timer::stop('search_api_gendev__prepare_items');

          $details['local_data'] = [
            '#type' => 'details',
            '#title' => $this->t('Local data'),
            '#open' => TRUE,
            '#description' => $this->t('Prepare time: %time', ['%time' => $time['time'] . ' ms']),
          ];
          $details['local_data'][] = $this->getItemOutput(reset($items));

          $details['actions'] = [
            '#type' => 'details',
            '#title' => $this->t('Actions'),
          ];
          $details['actions'][] = \Drupal::formBuilder()->getForm(ItemForm::class, $entity, $datasource);

          $output[$index->id() . '__' . $datasource->getPluginId()] = $details;
        }
      }
    }

    return $output;
  }

  /**
   * Create the debugging output from an Item.
   */
  protected function getItemOutput(ItemInterface $item) {
    $fields = [];
    foreach ($item->getFields() as $field) {
      $fields[$field->getPropertyPath()] = $field->getValues();
    }

    ksort($fields);

    $out = $this->dumper->exportAsRenderable($fields);

    if ($extra_data = $item->getAllExtraData()) {
      $out[] = $this->dumper->exportAsRenderable($extra_data);
    }

    return $out;
  }

}
