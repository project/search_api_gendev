<?php

namespace Drupal\search_api_gendev\Routing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    /** @var \Drupal\search_api\IndexInterface[] $indexes */
    $indexes = $this->entityTypeManager->getStorage('search_api_index')->loadByProperties(['status' => TRUE]);
    foreach ($indexes as $index) {
      foreach ($index->getEntityTypes() as $entity_type_id) {
        $route = new Route("/devel/$entity_type_id/{{$entity_type_id}}/search-api");
        $route
          ->addRequirements([
            '_permission' => 'access devel information',
          ])
          ->setOption('_admin_route', TRUE)
          ->setOption('_devel_entity_type_id', $entity_type_id)
          ->setOption('parameters', [
            $entity_type_id => ['type' => 'entity:' . $entity_type_id],
          ])
          ->addDefaults([
            '_controller' => '\Drupal\search_api_gendev\Controller\ItemController::entitySearchApi',
            '_title' => 'Devel search api',
          ]);

        $collection->add("entity.$entity_type_id.devel_search_api", $route);
      }
    }
  }

}
