<?php

namespace Drupal\search_api_gendev\Form;

use Drupal\Component\Utility\Timer;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api_gendev\SearchApiGendev;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Search API generic devel form.
 */
class ItemForm extends FormBase {

  /**
   * Search API Gendev service instance.
   *
   * @var \Drupal\search_api_gendev\SearchApiGendev
   */
  protected $searchApiGendev;

  /**
   * Language manager instance.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @var EntityInterface
   */
  protected $entity;

  /**
   * ItemForm constructor.
   */
  public function __construct(SearchApiGendev $search_api_gendev, LanguageManagerInterface $language_manager) {
    $this->searchApiGendev = $search_api_gendev;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('search_api_gendev'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'search_api_gendev_item';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityInterface $entity = NULL, DatasourceInterface $datasource = NULL) {
    $this->entity = $entity;

    if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
      $form['langcodes'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Languages'),
        '#required' => TRUE,
        '#options' => [],
      ];

      foreach ($entity->getTranslationLanguages() as $language) {
        $form['langcodes']['#options'][$language->getId()] = $language->getName();
      }

      $form['langcodes']['#default_value'] = array_keys($form['langcodes']['#options']);
    }

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['index'] = [
      '#type' => 'submit',
      '#value' => $this->t('Index item'),
      '#submit' => ['::submitIndex'],
    ];
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete item'),
      '#submit' => ['::submitDelete'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // NOP.
  }

  /**
   * Get the entity from the FormState.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  public function getSettings(array &$form, FormStateInterface $form_state): array {
    return [
      'langcodes' => array_filter($form_state->getValue('langcodes', [])),
    ];
  }

  /**
   * Submit handler for index.
   */
  public function submitIndex(array &$form, FormStateInterface $form_state) {
    Timer::start(__CLASS__ . '.' . __FUNCTION__);
    $items = $this->searchApiGendev->indexEntity($this->getEntity(), $this->getSettings($form, $form_state));
    $time = Timer::read(__CLASS__ . '.' . __FUNCTION__);

    $this->messenger()->addStatus($this->t("Indexed: %items.<br>Time: %time ms.", [
      '%items' => implode(', ', $items),
      '%time' => $time,
    ]));
  }

  /**
   * Submit handler for delete.
   */
  public function submitDelete(array &$form, FormStateInterface $form_state) {
    Timer::start(__CLASS__ . '.' . __FUNCTION__);
    $items = $this->searchApiGendev->deleteEntity($this->getEntity(), $this->getSettings($form, $form_state));
    $time = Timer::read(__CLASS__ . '.' . __FUNCTION__);

    $this->messenger()->addStatus($this->t("Deleted items: %items.<br>Time: %time ms.", [
      '%items' => implode(', ', $items),
      '%time' => $time,
    ]));
  }

}
